// Made changes to blob storage files
import com.azure.storage.blob.BlobClient;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import com.microsoft.azure.storage.blob.ListBlobItem;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.io.*;
import java.lang.reflect.Array;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.*;

public class locMenuAPIUploadsToCosmosMongo {

    private static final String storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=msridhartestsa;AccountKey=oQfNooZPf9DJRaXk7ROrHLwvqmxPxeEwcp2OAh7P89bjbLzJVy+S8p2xZAC8Fr7jZLOK6qkBTNyrLifNwgW9wQ==;EndpointSuffix=core.windows.net";
    public static void practiceMongoAPICalls(MongoDatabase mongoDatabase, MongoCollection<Document> mongoCollection) throws IOException {
        // We can then iterate through the list of collection names by getting all of them from the database object and printing them as strings
        for (String name : mongoDatabase.listCollectionNames()) {
            System.out.println(name);
        }
        // We can count the total number of documents stored in this collection with the countDocuments method and print it out (similar to select count(*) from table name query)
        System.out.println(mongoCollection.countDocuments());
        int count = 0;
        // We can iterate through all the documents in the particular collection specified to get their details
        // First we need to create a cursor object that can store the iterator we specify on all the documents (by find method on collection)
        // Then, as long as the cursor is not empty, we need to retrieve the value it has in it (next method) and store it as a Document
        // The document's values (doc.values()) needs to then be converted to an ArrayList<Object> type so we can retrieve the information from each object via a while/for loop
        // We can use the list item.get(index) command as for regular array lists to retrieve these objects and convert them to string format (this is similar to select * from table name query)
        try (MongoCursor<Document> cur = mongoCollection.find().iterator()) {

            while (cur.hasNext()) {
                count = count + 1;
                System.out.println("Count:" + count);
                Document doc = cur.next();
                ArrayList<java.lang.Object> cars = new ArrayList<>(doc.values());
                if (count == 1) {
                    System.out.printf("%s: %s%n : %s%n", cars.get(0), cars.get(1), cars.get(2));
                }
            }
        }
        // We can upload JSON files (documents) to Mongo DB API by parsing out the file and storing it as a string
        // Then we can use the Document object type to parse the string and store it in a Document object
        // We can upload the Document object using the insertOne method on the collection we've specified earlier. (similar to insert into tableName (values) query)
        File file = new File("/Users/msridhar/IdeaProjects/PerformanceTestAutomation/Payload.txt");
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line = bufferedReader.readLine();
        String jsonString = "";
        while(line != null) {
            jsonString = jsonString + line;
            line = bufferedReader.readLine();
        }
        Document document = Document.parse(jsonString);
        mongoCollection.insertOne(document);
        System.out.println("Document inserted into products collection.");
    }

    // This main method details the project in which the JSON location/menu api file is retrieved from Blob Storage and uploaded to Mongo DB with only certain fields
    // Create an html page with tables representing the CSV output
    public static void createHTMLWithDataOutput() throws IOException {
        File outputFile = new File(System.getProperty("user.dir") + "/index.html");
        // File outputFile = new File("https://gitlab.com/msridhar9/mongodbdatauploads/-/blob/master/index.html");
        FileWriter writer = new FileWriter(outputFile);
        writer.write("<html><body><br>");
        writer.write("<h3>Test</h3>");
//        LocalDate date = LocalDate.now();
//        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("MM-dd-yyyy");
//        String current_date = date.format(myFormatObj);
//        writer.write("<h3>" + current_date + "</h3><br>");
        writer.write("<p>It works!</p>");
        writer.write("</body></html>\n");
        writer.close();
        FileReader fileReader = new FileReader(outputFile);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line = bufferedReader.readLine();
        while(line != null) {
            System.out.println(line);
            line = bufferedReader.readLine();
        }
    }

    public static void main(String[] args) throws IOException, URISyntaxException, InvalidKeyException, StorageException {
        // First we need to create a client uri object to submit the mongo db client connection string
        MongoClientURI mongoClientURI = new MongoClientURI("mongodb://testdb101:a1XgMBDJLcVehb6yeZUqdJi4tBCCvDJfPx9Al6g0QtIOdyaxvCBQocWvzx8Wbo4tio9PCHZZhe4gj5X34A4Mzw==@testdb101.mongo.cosmos.azure.com:10255/?ssl=true&replicaSet=globaldb&retrywrites=false&maxIdleTimeMS=120000&appName=@testdb101@");
        // Then we can create a mongo client using this uri
        com.mongodb.MongoClient mongoClient = new MongoClient(mongoClientURI);
        // We should then create a mongo database object to get the database that we desire to work on
        MongoDatabase mongoDatabase = mongoClient.getDatabase("testdb101");
        // Create a cloud storage account object named storage account with the connection string
        CloudStorageAccount storageAccount = CloudStorageAccount.parse(storageConnectionString);
        // Create a cloud blob client object with the storage account
        CloudBlobClient cloudBlobClient = storageAccount.createCloudBlobClient();
        // Get the specific container's reference by supplying its name to the cloud blob client and store it in a cloud blob container object named container
        int count = 0;
        String blobFileName = "";
        String fileName = "";
        boolean noContainerMatchFlag = false;
        for (CloudBlobContainer container: cloudBlobClient.listContainers()) {
            System.out.println(container.getName().toString());
            for(ListBlobItem listBlobItem: container.listBlobs()) {
                count = count + 1;
                CloudBlockBlob blob = null;
                if (container.getName().equals("product-details")) {
                    // Get the specific blob's reference by supplying its name to the cloud blob container object created
                    blobFileName = "product_" + count + ".txt";
                    blob = container.getBlockBlobReference(blobFileName);
                    fileName = "products";
                } else if (container.getName().equals("product-configurations")) {
                    blobFileName = "config_" + count + ".txt";
                    blob = container.getBlockBlobReference(blobFileName);
                    fileName = "product_configurations";
                } else if (container.getName().equals("store-prices")) {
                    blobFileName = "storeprices_" + count + ".txt";
                    blob = container.getBlockBlobReference(blobFileName);
                    fileName = "store_prices";
                } else if (container.getName().equals("location-details")) {
                    blobFileName = "location_" + count + ".txt";
                    blob = container.getBlockBlobReference(blobFileName);
                    fileName = "location_details";
                } else if (container.getName().equals("location-taxes")) {
                    blobFileName = "locationtaxes_" + count + ".txt";
                    blob = container.getBlockBlobReference(blobFileName);
                    fileName = "location_taxes";
                }
                else {
                    noContainerMatchFlag = true;
                    break;
                }
//                CloudBlobContainer container = cloudBlobClient.getContainerReference("menu-additions");
//                // Get the specific blob's reference by supplying its name to the cloud blob container object created
//                CloudBlockBlob blob = container.getBlockBlobReference("RoastBeefSandwich.txt");
                // Download the file into the local system from the blob (using the downloadToFile path on the blob object)

                blob.downloadToFile(System.getProperty("user.dir") + "/" + blobFileName);
                // Convert the file into a string and then to a document object to upload on to the collection specified
                FileReader fileReader = new FileReader(new File(System.getProperty("user.dir") + "/" + blobFileName));
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                String line = bufferedReader.readLine();
                String jsonString = "";
                while(line != null) {
                    jsonString = jsonString + line;
                    line = bufferedReader.readLine();
                }
                System.out.println(jsonString);
                Document document = Document.parse(jsonString);
                // We can get the particular collection we want to work with and store it in the mongo collection object
                MongoCollection<Document> mongoCollection = mongoDatabase.getCollection(fileName);
                mongoCollection.insertOne(document);
                System.out.println("Document inserted into " + fileName + " collection.");
                File curr_folder = new File(System.getProperty("user.dir"));
                File fileList[] = curr_folder.listFiles();
                // Delete all files in the current directory with these extensions to keep the directory clean
                for (File f : fileList) {
                    if (f.getName().endsWith(blobFileName)) {
                        f.delete();
                    }
                }
            }
            count = 0;
            if (noContainerMatchFlag) {
                noContainerMatchFlag = false;
                System.out.println(container.getName());
            }
        }
//        CloudBlobContainer container = cloudBlobClient.getContainerReference("menu-additions");
//        // Get the specific blob's reference by supplying its name to the cloud blob container object created
//        CloudBlockBlob blob = container.getBlockBlobReference("RoastBeefSandwich.txt");
//        // Download the file into the local system from the blob (using the downloadToFile path on the blob object)
//        blob.downloadToFile("/Users/msridhar/IdeaProjects/PerformanceTestAutomation/Menu-Addition.txt");
//        // Convert the file into a string and then to a document object to upload on to the collection specified
//        FileReader fileReader = new FileReader(new File("/Users/msridhar/IdeaProjects/PerformanceTestAutomation/Menu-Addition.txt"));
//        BufferedReader bufferedReader = new BufferedReader(fileReader);
//        String line = bufferedReader.readLine();
//        String jsonString = "";
//        while(line != null) {
//            jsonString = jsonString + line;
//            line = bufferedReader.readLine();
//        }
//        Document document = Document.parse(jsonString);
//        mongoCollection.insertOne(document);
//        System.out.println("Document inserted into products collection.");
        createHTMLWithDataOutput();
    }
}

